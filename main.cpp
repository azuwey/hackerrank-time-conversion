/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/20/2019
 * Source: HackerRank - https://www.hackerrank.com/challenges/time-conversion/problem
 **/
#include <bits/stdc++.h>

using namespace std;

string timeConversion(string s) {
  int hour = stoi(s.substr(0, 2));

  if (s[8] == 'A' && hour >= 12) {
    // 12:00 AM to 12:59 AM
    hour += -12;
  } else if (s[8] == 'P' && hour < 12 && hour >= 1) {
    // 01:00 PM to 11:59 PM
    hour += 12;
  }

  return (hour < 10 ? "0" : "") + to_string(hour) + s.substr(2, 6);
}

int main() {
  ofstream fout(getenv("OUTPUT_PATH"));

  string s;
  getline(cin, s);

  string result = timeConversion(s);

  fout << result << "\n";

  fout.close();

  return 0;
}
